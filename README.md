# Dolibar docker

Outil de gestion des adhérents. C'est à la base un CRM qui peut faire quelques éléments de comptabilité.

## Installation

L'installation est relativement automatisée mais il faut faire beaucoup de settings pour qu'il marche bien.

### dépendences:

nécessaire:
* [ansible docker host](https://framagit.org/cycles-manivelles/ansible-role_docker-host)

* [ansible traefik docker](https://framagit.org/cycles-manivelles/ansible-role_traefik-docker)

### Variables
Les variables sont dans `defaults/main.yml`

spécifiques

* **dolibarr_name** (défault: default) => prefix directory name
* **dolibarr_domain** => public domaine
* **dolibarr_admin_user** => First admin user
* **dolibarr_admin_password** => First admin user (gererated avoiding avoiding security breach in case variable not set)

variables secondaires
* **root_directory**: /srv
* **compose_directory**: "{{ root_directory }}/docker-compose/{{ dolibarr_name }}-dolibarr"
dolibarr_data: "{{ compose_directory }}/dolibarr-data"

database (mots de passes générés aléatoirement)
* **maria_dolibarr_backupfile**: "{{ compose_directory }}/db/backup/backup.sql"
* **db_bakcup_script**: "{{ compose_directory }}/backup-db.sh"
* **maria_root_password**: "{{ lookup('password', '{{ inventory_dir }}/{{ dolibarr_name }}.dolibarradmindb-pass length=50 chars=ascii_letters,digits,hexdigits') }}"
maria_dolibarr_password: "{{ lookup('password', '{{ inventory_dir }}/dolibarrdb-{{ dolibarr_name }}-pass length=25 chars=ascii_letters,digits,hexdigits') }}"
* **maria_dolibarr_dbname**: dolibarrdb
* **maria_dolibarr_user**: dolibardbuser
* **pg_rainloop_backupfile**: "{{ compose_directory }}/db/backup/backup.sql"
* **db_backup_script**: "{{ compose_directory }}/backup-db.sh"

versions

* **dolibbar_version**: "12.0.4-php7.4"
* **nginx_version**: "1.17.9"
* **mariadb_version**: "10.5.3"
